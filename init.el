(org-babel-load-file
 (expand-file-name
  "config.org"
  user-emacs-directory))

;; No tocar. Código generado en automático con la instalación de paquetes y con el uso de org-agenda.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ispell-dictionary nil)
 '(line-number-mode nil)
 '(org-agenda-files
   '("~/Documentos/org-mode/compras14mayo.org" "~/hola.org" "~/agenda.org"))
 '(package-selected-packages
   '(olivetti move-text doom-modeline solarized-theme magit try auctex use-package markdown-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
